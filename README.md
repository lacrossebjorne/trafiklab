##Arbetsprov för roll som sytemutvecklare hos SBAB

###The challenge

The task is to write an application to find out which bus lines that have the most bus
stops on their route, and to present the top 10 bus lines in a nice and formatted way
in a web browser.
The web page should show the names of every bus stop for each of the bus lines in
the top 10 list.
There are no requirements how the bus stops are sorted.
To accomplish the task please use the Trafiklab’s open API
(https://www.trafiklab.se/). You can find more information about the specific API on
the documentation page: https://www.trafiklab.se/api/sl-hallplatser-och-linjer-2.
You can register your own account at Trafiklab.
The data should be automatically gathered from API for each run of the application.

####Requirements

We would like you to use JavaScript for the front-end parts of the application and
Java for the back-end.
You are free to choose the version of JavaScript and Java.
You can use external libraries if you would like to.


###How to run it

####Prerquisites

* Java 17 or higher installed
* Internet access
* Web browser - Chromium based, Firefox or Safari

Navigate to the root of the project folder '.../arbetsprov'
Build the application using either Maven (mvn) or the included Maven Wrapper (mvnw linux, mvnw.cmd in windows)\
`mvn clean install`
or\
`./mvnw clean install` (./mvnw.cmd clean install on windows)
To run the application `java -jar target/arbetsprov-sbab-0.0.1-SNAPSHOT.jar`
Open a web browser and enter `http://localhost:8080/`


###The implementation

The backend is written in Java (java 17 & SpringBoot 2.6.3).\
Abbreviated it consists of an Api (Controller), Service and an Integration client that fetches data from the TrafikLab Api.\
The service filters, collects and order (desc) the ten bus lines with most stops.

The frontend is written with Typescript (Angular ~v13.2).\
It's lightweight and fetches the ordered bus lines and present them in a table like layout.\
Clicking a "line" expands the bus stops where you can scroll to see all the names.

###Assumptions and limitations

* I have deliberately excluded all tests.
* The TrafikLabService.getTopTenBusLinesByNoOfStops method is somewhat heavy and could (should) be broken down for flexibility and ease of testing.
* Error handling is kept to the out most necessary (see console for output).
* The application can only fetch bus data.
* In a real world application the data from TrafikLab would be cached and updated once per day
* As I interpreted the assignment I've summed up the total number of stop (outbound and inbound) for a bus line when comparing.
* There is a styling issue when using Safari: text is blurry when hovering a bus stop name 
* The application is not tested in Internet Explorer


