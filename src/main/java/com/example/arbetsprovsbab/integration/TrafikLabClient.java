package com.example.arbetsprovsbab.integration;

import com.example.arbetsprovsbab.exception.TrafikLabException;
import com.example.arbetsprovsbab.model.TrafikLabResultWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

@Controller
public class TrafikLabClient {

    private static final Logger LOG = LoggerFactory.getLogger(TrafikLabClient.class);

    @Value("${trafiklab.BASE_URL}")
    private String url;
    @Value("${trafiklab.API_KEY}")
    private String apiKey;

    @Autowired
    private ObjectMapper mapper;

    public TrafikLabResultWrapper getTrafikLabData(Map<String, String> queryParams) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(url)
                .queryParam("key", apiKey);
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            if (entry.getValue() != null) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
        }
        Response response = null;
        try {
            response = webTarget
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .acceptEncoding("gzip, deflate")
                    .get(Response.class);

            if (response.getStatus() != 200) {
                throw new TrafikLabException("Fel uppstod vid hämtning av " + queryParams.get("model"));
            }

            return mapper.readValue(response.readEntity(String.class), TrafikLabResultWrapper.class);

        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage());
            throw new TrafikLabException("Det gick inte processa svaret från TrafikLab");
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new TrafikLabException("Det gick inte att kontakta SL");
        } finally {
            if (response != null) {
                response.close();
            }
            if (client != null) {
                client.close();
            }
        }
    }

    private WebTarget getWebTarget(String model, String transportMode) {
        WebTarget target = ClientBuilder.newClient()
                .target(url)
                .queryParam("key", apiKey)
                .queryParam("model", model);
        if (transportMode != null) {
            target.queryParam("DefaultTransportModeCode", transportMode);
        }
        return target;
    }

}
