package com.example.arbetsprovsbab.model.dto;

import com.example.arbetsprovsbab.model.response.entity.JourneyPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LineDTO {

    private String lineNo;
    private List<JourneyPattern> outBoundJourney = new ArrayList<>();
    private List<JourneyPattern> inBoundJourney = new ArrayList<>();

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public List<JourneyPattern> getOutBoundJourney() {
        return outBoundJourney;
    }

    public void setOutBoundJourney(List<JourneyPattern> outBoundJourney) {
        this.outBoundJourney = outBoundJourney;
    }

    public List<JourneyPattern> getInBoundJourney() {
        return inBoundJourney;
    }

    public void setInBoundJourney(List<JourneyPattern> inBoundJourney) {
        this.inBoundJourney = inBoundJourney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LineDTO lineDTO = (LineDTO) o;
        return lineNo.equals(lineDTO.lineNo) && outBoundJourney.equals(lineDTO.outBoundJourney) && inBoundJourney.equals(lineDTO.inBoundJourney);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lineNo, outBoundJourney, inBoundJourney);
    }
}
