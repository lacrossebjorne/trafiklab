package com.example.arbetsprovsbab.model.response.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JourneyPattern {

    private String lineNumber;
    private Integer directionCode;
    private String stopPointNumber;
    private LocalDateTime existsFromDate;
    private LocalDateTime lastModifiedUtcDateTime;
    private String stopPointName;

    public JourneyPattern() {
    }

    @JsonIgnore
    @JsonGetter("lineNumber")
    public String getLineNumber() {
        return lineNumber;
    }

    @JsonSetter("LineNumber")
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    @JsonGetter("directionCode")
    public Integer getDirectionCode() {
        return directionCode;
    }

    @JsonSetter("DirectionCode")
    public void setDirectionCode(Integer directionCode) {
        this.directionCode = directionCode;
    }

    @JsonIgnore
    @JsonGetter("stopPointNumber")
    public String getStopPointNumber() {
        return stopPointNumber;
    }

    @JsonSetter("JourneyPatternPointNumber")
    public void setStopPointNumber(String stopPointNumber) {
        this.stopPointNumber = stopPointNumber;
    }

    @JsonIgnore
    @JsonGetter("existsFromDate")
    public LocalDateTime getExistsFromDate() {
        return existsFromDate;
    }

    @JsonSetter("ExistsFromDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    public void setExistsFromDate(LocalDateTime existsFromDate) {
        this.existsFromDate = existsFromDate;
    }

    @JsonIgnore
    @JsonGetter("lastModifiedUtcDateTime")
    public LocalDateTime getLastModifiedUtcDateTime() {
        return lastModifiedUtcDateTime;
    }

    @JsonSetter("LastModifiedUtcDateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    public void setLastModifiedUtcDateTime(LocalDateTime lastModifiedUtcDateTime) {
        this.lastModifiedUtcDateTime = lastModifiedUtcDateTime;
    }

    public String getStopPointName() {
        return stopPointName;
    }

    public void setStopPointName(String stopPointName) {
        this.stopPointName = stopPointName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JourneyPattern that = (JourneyPattern) o;
        return lineNumber.equals(that.lineNumber) && directionCode.equals(that.directionCode) && stopPointNumber.equals(that.stopPointNumber) && existsFromDate.equals(that.existsFromDate) && lastModifiedUtcDateTime.equals(that.lastModifiedUtcDateTime) && stopPointName.equals(that.stopPointName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lineNumber, directionCode, stopPointNumber, existsFromDate, lastModifiedUtcDateTime, stopPointName);
    }

    @Override
    public String toString() {
        return "JourneyPattern{" +
                "lineNumber=" + lineNumber +
                ", directionCode=" + directionCode +
                ", stopPointNumber=" + stopPointNumber +
                ", existsFromDate=" + existsFromDate +
                ", lastModifiedUtcDateTime=" + lastModifiedUtcDateTime +
                ", stopPointName='" + stopPointName + '\'' +
                '}';
    }
}
