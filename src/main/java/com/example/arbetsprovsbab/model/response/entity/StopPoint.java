package com.example.arbetsprovsbab.model.response.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StopPoint {

    private String stopPointNumber;
    private String stopPointName;
    private String stopAreaNumber;
    private String stopAreaTypeCode;

    public StopPoint() {
    }

    @JsonGetter("stopPointNumber")
    public String getStopPointNumber() {
        return stopPointNumber;
    }

    @JsonSetter("StopPointNumber")
    public void setStopPointNumber(String stopPointNumber) {
        this.stopPointNumber = stopPointNumber;
    }

    @JsonGetter(value = "stopPointName")
    public String getStopPointName() {
        return stopPointName;
    }

    @JsonSetter(value = "StopPointName")
    public void setStopPointName(String stopPointName) {
        this.stopPointName = stopPointName;
    }

    @JsonIgnore
    @JsonGetter(value = "stopAreaNumber")
    public String getStopAreaNumber() {
        return stopAreaNumber;
    }

    @JsonSetter(value = "StopAreaNumber")
    public void setStopAreaNumber(String stopAreaNumber) {
        this.stopAreaNumber = stopAreaNumber;
    }

    @JsonIgnore
    @JsonGetter(value = "stopAreaTypeCode")
    public String getStopAreaTypeCode() {
        return stopAreaTypeCode;
    }

    @JsonSetter(value = "StopAreaTypeCode")
    public void setStopAreaTypeCode(String stopAreaTypeCode) {
        this.stopAreaTypeCode = stopAreaTypeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StopPoint stopPoint = (StopPoint) o;
        return stopPointNumber.equals(stopPoint.stopPointNumber) && stopPointName.equals(stopPoint.stopPointName) && stopAreaNumber.equals(stopPoint.stopAreaNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stopPointNumber, stopPointName, stopAreaNumber);
    }
}
