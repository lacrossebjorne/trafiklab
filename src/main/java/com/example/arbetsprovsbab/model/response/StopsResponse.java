package com.example.arbetsprovsbab.model.response;

import com.example.arbetsprovsbab.model.response.entity.StopPoint;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class StopsResponse extends ResponseData {

    @JsonProperty(value = "Result")
    private List<StopPoint> stops;

    public StopsResponse() {
    }

    public List<StopPoint> getStops() {
        return stops;
    }

    public void setStops(List<StopPoint> stops) {
        this.stops = stops;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StopsResponse that = (StopsResponse) o;
        return stops.equals(that.stops);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stops);
    }
}
