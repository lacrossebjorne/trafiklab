package com.example.arbetsprovsbab.model.response;

import com.example.arbetsprovsbab.model.response.entity.JourneyPattern;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JourneyResponse extends ResponseData {

    @JsonProperty(value = "Result")
    private List<JourneyPattern> journeys = new ArrayList<>();

    public JourneyResponse() {
    }

    public List<JourneyPattern> getJourneys() {
        return journeys;
    }

    public void setJourneys(List<JourneyPattern> journeys) {
        this.journeys = journeys;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JourneyResponse that = (JourneyResponse) o;
        return journeys.equals(that.journeys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(journeys);
    }
}
