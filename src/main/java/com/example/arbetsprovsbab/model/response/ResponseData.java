package com.example.arbetsprovsbab.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = JourneyResponse.class, name = "JourneyPatternPointOnLine"),
        @JsonSubTypes.Type(value = StopsResponse.class, name = "StopPoint")
})
public class ResponseData {

    @JsonProperty(value = "Version")
    private String version;


    public ResponseData() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
