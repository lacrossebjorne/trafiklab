package com.example.arbetsprovsbab.model;

import com.example.arbetsprovsbab.model.response.ResponseData;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class TrafikLabResultWrapper {

        @JsonProperty(value = "StatusCode")
        private Integer statusCode;
        @JsonProperty(value = "Message")
        private String message;
        @JsonProperty(value = "ExecusionTime")
        private Integer execusionTime;
        @JsonProperty(value = "ResponseData")
        ResponseData responseData;

        public TrafikLabResultWrapper() {
        }

        public Integer getStatusCode() {
                return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
                this.statusCode = statusCode;
        }

        public String getMessage() {
                return message;
        }

        public void setMessage(String message) {
                this.message = message;
        }

        public Integer getExecusionTime() {
                return execusionTime;
        }

        public void setExecusionTime(Integer execusionTime) {
                this.execusionTime = execusionTime;
        }

        public ResponseData getResponseData() {
                return responseData;
        }

        public void setResponseData(ResponseData responseData) {
                this.responseData = responseData;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                TrafikLabResultWrapper that = (TrafikLabResultWrapper) o;
                return statusCode.equals(that.statusCode) && message.equals(that.message) && execusionTime.equals(that.execusionTime) && responseData.equals(that.responseData);
        }

        @Override
        public int hashCode() {
                return Objects.hash(statusCode, message, execusionTime, responseData);
        }
}


