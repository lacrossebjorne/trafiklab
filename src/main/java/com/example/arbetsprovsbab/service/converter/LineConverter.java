package com.example.arbetsprovsbab.service.converter;

import com.example.arbetsprovsbab.model.dto.LineDTO;
import com.example.arbetsprovsbab.model.response.entity.JourneyPattern;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class LineConverter {
    private static final Integer OUTBOUND = 1;
    private static final Integer INBOUND = 2;

    public List<LineDTO> convertToLines(Map<String, List<JourneyPattern>> topTenJourney) {
        List<LineDTO> topTenLines = new LinkedList<>();
        for (Map.Entry<String, List<JourneyPattern>> jour: topTenJourney.entrySet()) {
            LineDTO line = new LineDTO();
            line.setLineNo(jour.getKey());
            line.setOutBoundJourney(jour.getValue().stream().filter(j -> OUTBOUND.equals(j.getDirectionCode())).toList());
            line.setInBoundJourney(jour.getValue().stream().filter(j -> INBOUND.equals(j.getDirectionCode())).toList());
            topTenLines.add(line);
        }
        return topTenLines;
    }
}
