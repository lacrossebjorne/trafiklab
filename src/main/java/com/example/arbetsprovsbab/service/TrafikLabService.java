package com.example.arbetsprovsbab.service;

import com.example.arbetsprovsbab.exception.TrafikLabException;
import com.example.arbetsprovsbab.integration.TrafikLabClient;
import com.example.arbetsprovsbab.model.TrafikLabResultWrapper;
import com.example.arbetsprovsbab.model.response.JourneyResponse;
import com.example.arbetsprovsbab.model.response.StopsResponse;
import com.example.arbetsprovsbab.model.response.entity.JourneyPattern;
import com.example.arbetsprovsbab.model.response.entity.StopPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@Service
public class TrafikLabService {

    private static final Logger LOG = LoggerFactory.getLogger(TrafikLabService.class);

    private List<StopPoint> stopPoints = new ArrayList<>();
    private List<JourneyPattern> busJourneyPatterns = new ArrayList<>();

    @Autowired
    TrafikLabClient client;

    public Map<String, List<JourneyPattern>> getTopTenBusLinesByNoOfStops() throws TrafikLabException {
        this.stopPoints = getStopPoints();
        this.busJourneyPatterns = getAllBusJourneyPatterns();
        LOG.info("Filter if existFromDate > today");
        List<JourneyPattern> filtered = this.busJourneyPatterns.stream().filter(j -> j.getExistsFromDate().isBefore(LocalDateTime.now())).toList();

        LOG.info("Collect and sort by line with most total stops");
        Map<String, List<JourneyPattern>> topTen = filtered.stream()
                .collect(groupingBy(JourneyPattern::getLineNumber))
                .entrySet()
                .stream()
                .sorted(Comparator.comparingInt(e -> -e.getValue().size()))
                .limit(10)
                .collect(toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> {
                            throw new AssertionError();
                        },
                        LinkedHashMap::new
                ));
        LOG.info("Set name for each stopPoint");
        topTen.forEach((key, value) -> value.forEach(j -> {
                    try {
                        j.setStopPointName(getStopPointName(j.getStopPointNumber()));
                    } catch (NoSuchElementException e) {
                        j.setStopPointName("Hållsplats utan namn");
                        LOG.error(j.toString());
                    }
                })
        );
        return topTen;
    }

    public List<JourneyPattern> getAllBusJourneyPatterns() {
        LOG.info("Fetching journeypatterns");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("model", "jour");
        queryParams.put("DefaultTransportModeCode", "BUS");
        TrafikLabResultWrapper resultWrapper = this.client.getTrafikLabData(queryParams);
        JourneyResponse responseData = (JourneyResponse) resultWrapper.getResponseData();
        if (responseData != null && !responseData.getJourneys().isEmpty()) {
            return responseData.getJourneys();
        } else {
            throw new TrafikLabException("Det gick inte att hämta data för busslinjer. Försök igen senare.");
        }
    }

    private String getStopPointName(String stopPointNumber) throws NoSuchElementException {
        if (stopPointNumber == null) {
            throw new TrafikLabException("StopPointNumber är inte angivet");
        }
        return this.stopPoints.stream()
                .filter(stopPoint -> stopPoint.getStopPointNumber().equals(stopPointNumber))
                .findFirst().orElseThrow().getStopPointName();
    }

    public List<StopPoint> getStopPoints() {
        LOG.info("Fetching stopPoints");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("model", "stop");
        TrafikLabResultWrapper resultWrapper = this.client.getTrafikLabData(queryParams);
        StopsResponse responseData = (StopsResponse) resultWrapper.getResponseData();
        if (responseData != null && !responseData.getStops().isEmpty()) {
            return responseData.getStops()
                    .stream()
                    .filter(stop -> "BUSTERM".equals(stop.getStopAreaTypeCode()))
                    .toList();
        } else {
            throw new TrafikLabException("Det gick inte att hämta hållplatsdata. Försök igen senare.");
        }
    }

}
