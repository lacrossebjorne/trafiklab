package com.example.arbetsprovsbab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArbetsprovSbabApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArbetsprovSbabApplication.class, args);
    }

}
