package com.example.arbetsprovsbab.controller;

import com.example.arbetsprovsbab.model.dto.LineDTO;
import com.example.arbetsprovsbab.service.TrafikLabService;
import com.example.arbetsprovsbab.service.converter.LineConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api")
public class TrafikLabController {

    @Autowired
    TrafikLabService service;

    @Autowired
    LineConverter lineConverter;

    @GetMapping("topten")
    public List<LineDTO> getTopTen() {
        return lineConverter.convertToLines(service.getTopTenBusLinesByNoOfStops());
    }

}
