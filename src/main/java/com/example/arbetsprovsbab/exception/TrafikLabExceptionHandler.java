package com.example.arbetsprovsbab.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TrafikLabExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {TrafikLabException.class})
    protected ResponseEntity<Object> handleError(RuntimeException e, WebRequest request) {
        String responseBody = "Ett fel har uppstått";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
