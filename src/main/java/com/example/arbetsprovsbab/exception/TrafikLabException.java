package com.example.arbetsprovsbab.exception;

public class TrafikLabException extends RuntimeException {

    public TrafikLabException() {
        super();
    }

    public TrafikLabException(String msg) {
        super(msg);
    }
}
