import {JourneyPattern} from "./journey-pattern";

export class Line {
  lineNo: string;
  outBoundJourney: JourneyPattern[];
  inBoundJourney: JourneyPattern[];


  constructor(lineNo: string, outBoundJourney: JourneyPattern[], inBoundJourney: JourneyPattern[]) {
    this.lineNo = lineNo;
    this.outBoundJourney = outBoundJourney;
    this.inBoundJourney = inBoundJourney;
  }
}
