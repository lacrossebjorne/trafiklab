import { TestBed } from '@angular/core/testing';

import { TrafikLabService } from './trafik-lab.service';

describe('TrafikLabService', () => {
  let service: TrafikLabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrafikLabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
