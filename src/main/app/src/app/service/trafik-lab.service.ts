import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, EMPTY, Observable, Subject, tap} from "rxjs";
import {environment} from "../../environments/environment";
import {Line} from "../model/line";

@Injectable({
  providedIn: 'root'
})
export class TrafikLabService {
  private readonly url = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getLinesWithMostStops(): Observable<Line[]> {
    return this.http.get<Line[]>(`${this.url}/topten`);
  }
}
