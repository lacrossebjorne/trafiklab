import {Component, OnInit} from '@angular/core';
import {TrafikLabService} from "./service/trafik-lab.service";
import {catchError, EMPTY, Observable, Subject} from "rxjs";
import {Line} from "./model/line";
import {KeyValue} from "@angular/common";
import {JourneyPattern} from "./model/journey-pattern";
import {StopPoint} from "./model/stop-point";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  lines$: Observable<Line[]> | undefined;
  stops$: Observable<StopPoint[]> | undefined;
  tableHeaders = ['Linje', 'Riktning', 'Hållplatser'];
  expanded: boolean[] = [];

  loadingError$ = new Subject<boolean>();

  constructor(private trafikLabService: TrafikLabService) {

  }

  ngOnInit() {
    this.lines$ = this.trafikLabService.getLinesWithMostStops()
      .pipe(
        catchError(err => {
          console.log('Det gick inte att hämta busslinjer med flest hållplatser.', err);
          this.loadingError$.next(true);
          return EMPTY;
        })
      );;
  }

  handleError(operation: string): (error: any) => Observable<any> {
    return (error: any): Observable<any> => {
      console.log(`${operation}`, error);
      this.loadingError$.next(true);
      return EMPTY;
    }
  }

  toggleExpanded(index: number) {
    this.expanded[index] = !this.expanded[index];
  }
}
